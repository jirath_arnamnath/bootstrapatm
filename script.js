$(document).ready(function() {
	var account = {
		'1234' : {'pin': '1234', 'balance': 500},
		'5678' : {'pin': '5678', 'balance': 1500},
		'0000' : {'pin': '0000', 'balance': 2500}
	};
	
	$('.form-menu').hide();
	
	$('.form-signin').submit(function() {
		var accountNo = $('#accountNo').val();
		var pin = $('#pin').val();
		if (accountNo != null && account[accountNo]['pin'] == pin) {
			$('.form-menu').show();
			$('.form-signin').hide();
			showbalance(accountNo);

			$('#deposit').click(function(){
				var num = parseInt(prompt("ป้อนจำนวนเงินที่ต้องการฝาก"));
				if (isNaN(num) || num <= 0){
					alert("กรุณาป้อนข้อมูลให้ถูกต้อง !");
				} else {
					deposit(accountNo, num);
				}
			});

			$('#withdraw').click(function(){
				var num = parseInt(prompt("ป้อนจำนวนเงินที่ต้องการถอน"));
				if (isNaN(num) || num <= 0){
					alert("กรุณาป้อนข้อมูลให้ถูกต้อง !");
				} else {
					if (num <= account[accountNo]['balance']){
						withdraw(accountNo, num);
					}else{
						alert("จำนวนเงินในการถอนไม่เพียงพอ !");
					}
				}
			});

		} else {
			alert('เลขที่บัญชีหรือรหัสผ่านไม่ถูกต้อง');
		}
		return false;
	});

	$('.form-menu').submit(function() {
		$('#accountNo').clear();
		$('#pin').clear();
		$('.form-signin').show();
		$('.form-menu').hide();
	});
	
	function showbalance(accountNo) {
		$("#user").html('เลขที่บัญชี : ' + accountNo);
		$("#balance").html('จำนวนเงิน : ' + account[accountNo]['balance'].toString() + ' บาท');
	}

	function deposit(accountNo, amount) {
		account[accountNo]['balance'] += amount;
		showbalance(accountNo);
		alert('ทำรายการฝากเงินเรียบร้อยแล้ว');
	}

	function withdraw(accountNo, amount) {
		account[accountNo]['balance'] -= amount;
		showbalance(accountNo);
		alert('ทำรายการถอนเงินเรียบร้อยแล้ว');
	}
});